import { RouterModule, Routes } from "@angular/router";
import { AcercaComponent } from "./components/acerca/acerca.component";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { InicioComponent } from "./components/inicio/inicio.component";


import { HomeComponent } from "./components/home/home.component";
import { UbicacionComponent } from "./components/ubicacion/ubicacion.component";
import { RedesComponent } from "./components/redes/redes.component";
import { MasComponent } from "./components/mas/mas.component";
import { AyudaComponent } from "./components/ayuda/ayuda.component";

const APP_ROUTES: Routes = [
  {path: 'inicio' , component: InicioComponent},
  {path: 'acerca', component:AcercaComponent},
  {path: 'contacto', component:ContactoComponent},
  {path: 'home',component:HomeComponent},
  {path: 'ubicacion',component:UbicacionComponent},
  {path: 'redes',component:RedesComponent},
  {path: 'mas',component:MasComponent},
  {path:'ayuda',component:AyudaComponent},

  {path: '**',pathMatch:'full',redirectTo:'inicio'}
];

export const APP_ROUTING= RouterModule.forRoot(APP_ROUTES);
